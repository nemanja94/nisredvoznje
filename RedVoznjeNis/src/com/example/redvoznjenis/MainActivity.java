package com.example.redvoznjenis;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends Activity implements OnItemSelectedListener {

	private enum Dan {
		RADNI, SUBOTA, NEDELJA
	}

	private Spinner spinner;
	private String[] vremena;
	private String trenutnaLinija;
	private ListView l1;
	private int redniBroj;
	ArrayAdapter<CharSequence> adapter; // Za izbor linija
	ArrayAdapter<String> adapter1; // Za listu vremena
	private String[] linije;
	private String[] linijeB;
	private boolean smerA;
	TextView tr;
	private int index;
	private Dan dan;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Intent intent = getIntent();
		this.setTitle(getString(R.string.naslov));
		Spinner spinner = (Spinner) findViewById(R.id.linijeNis);
		spinner.setOnItemSelectedListener(this);
		l1 = (ListView) findViewById(R.id.ni_list);
		adapter = ArrayAdapter.createFromResource(this, R.array.linijeNis,
				R.layout.list_layout);
		adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		spinner.setAdapter(adapter);
		linije = getResources().getStringArray(R.array.linijeNis);
		linijeB = getResources().getStringArray(R.array.linijeNisB);
		tr = (TextView) findViewById(R.id.trLinija);
		smerA = false;
		dan = Dan.RADNI;
		prikazi();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.info) {
			info();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void info() {
		new AlertDialog.Builder(this).setTitle(R.string.txtInfo)
				.setMessage(R.string.txtInfoTekst)
				.setNegativeButton(R.string.no, null).show();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		trenutnaLinija = linije[position];
		tr.setText("Trenutna linija:\n" + trenutnaLinija);
		smerA = false;
		index = position;
		prikazi();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		trenutnaLinija = linije[0];
		tr.setText("Trenutna linija:\n" + trenutnaLinija);
		smerA = false;
		index = 0;
		prikazi();
	}

	public void promeniSmer(View v) {
		if (smerA) {
			tr.setText("Trenutna linija:\n" + linije[index]);
			smerA = false;
			prikazi();
		} else {
			tr.setText("Trenutna linija:\n" + linijeB[index]);
			smerA = true;
			prikaziB();
		}
	}

	public void postaviDan(View view) {
		boolean checked = ((RadioButton) view).isChecked();

		switch (view.getId()) {
		case R.id.radniDan:
			if (checked) {
				dan = Dan.RADNI;
				if (!smerA) {
					prikazi();
				} else {
					prikaziB();
				}
			}
			break;
		case R.id.subota:
			if (checked) {
				dan = Dan.SUBOTA;
				if (!smerA) {
					prikazi();
				} else {
					prikaziB();
				}
			}
			break;
		case R.id.nedelja:
			if (checked) {
				dan = Dan.NEDELJA;
				if (!smerA) {
					prikazi();
				} else {
					prikaziB();
				}
			}
			break;
		}
	}

	public void prikazi() {
		if (index == 0) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl1ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl1sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl1na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 1) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl2ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl2sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl2na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 2) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl3ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl3sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl3na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 3) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl4ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl4sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl4na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 4) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl5ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl5sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl5na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 5) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl6ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl6sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl6na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 6) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl7ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl7sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl7na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 7) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl8ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl8sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl8na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 8) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl9ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl9sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl9na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 9) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl10ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl10sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl10na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 10) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl12ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl12sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl12na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 11) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl13ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl13sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl13na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 12) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl34ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl34sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl34na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 13) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl36ra);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl36sa);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl36na);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		}
	}

	public void prikaziB() {
		if (index == 0) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl1rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl1sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl1nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 1) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl2rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl2sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl2nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 2) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl3rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl3sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl3nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 3) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl4rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl4sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl4nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 4) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl5rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl5sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl5nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 5) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl6rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl6sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl6nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 6) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl7rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl7sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl7nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 7) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl8rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl8sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl8nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 8) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl9rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl9sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl9nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 9) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl10rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl10sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl10nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 10) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl12rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl12sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl12nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 11) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl13rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl13sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl13nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 12) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl34rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl34sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl34nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		} else if (index == 13) {
			if (dan == Dan.RADNI) {
				vremena = getResources().getStringArray(R.array.nl36rb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.SUBOTA) {
				vremena = getResources().getStringArray(R.array.nl36sb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			} else if (dan == Dan.NEDELJA) {
				vremena = getResources().getStringArray(R.array.nl36nb);
				adapter1 = new ArrayAdapter<String>(this, R.layout.list_layout,
						R.id.listTextView, vremena);
				l1.setAdapter(adapter1);
			}
		}
	}
	
	 /**
     * This class makes the ad request and loads the ad.
     */
    public static class AdFragment extends Fragment {

        private AdView mAdView;

        public AdFragment() {
        }

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);

            // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
            // values/strings.xml.
            mAdView = (AdView) getView().findViewById(R.id.adView);

            // Create an ad request. Check logcat output for the hashed device ID to
            // get test ads on a physical device. e.g.
            // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.id.adView, container, false);
        }

        /** Called when leaving the activity */
        @Override
        public void onPause() {
            if (mAdView != null) {
                mAdView.pause();
            }
            super.onPause();
        }

        /** Called when returning to the activity */
        @Override
        public void onResume() {
            super.onResume();
            if (mAdView != null) {
                mAdView.resume();
            }
        }

        /** Called before the activity is destroyed */
        @Override
        public void onDestroy() {
            if (mAdView != null) {
                mAdView.destroy();
            }
            super.onDestroy();
        }

    }
}
